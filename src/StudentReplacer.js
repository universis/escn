import { ApplicationService } from '@themost/common';
import { SchemaLoaderStrategy } from '@themost/data';
import path from 'path';

export class StudentReplacer extends ApplicationService {
    constructor(app) {
        super(app);
    }

    apply() {
        // get schema loader
        const schemaLoader = this.getApplication().getConfiguration().getStrategy(SchemaLoaderStrategy);
        // get model definition
        const model = schemaLoader.getModelDefinition('Student');

        model.eventListeners = model.eventListeners || [];
        
        // add the listeners
        model.eventListeners.push(
            {type: path.resolve(__dirname, 'listeners/AfterSaveInsertStudentListener')},
            {type: path.resolve(__dirname, 'listeners/DeactivateCardOnStudentStatusUpdateListener')},
            {type: path.resolve(__dirname, 'listeners/CreateCardOnStudentStatusUpdateListener')}
        );
        
        schemaLoader.setModelDefinition(model);
    }
}

