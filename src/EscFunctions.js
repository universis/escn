// const common = require("@themost/common");
import { DataNotFoundError } from '@themost/common';
import { EscnFactory } from '@universis/escn-nodejs-generator';

export async function escn(context, student) {
    // Get student data
    student = await context.model('Student')
        .where('id')
        .equal(student)
        .expand('department')
        .select('id', 'department/organization')
        .getItem();

    if (student == null) {
        throw new DataNotFoundError(
            'The specified student cannot be found or is inaccesible'
        );
    }
    
    // Get the academic card configuration of the student's institute
    const academicCardConfiguration = await context.model('AcademicCardConfiguration')
        .where('institute')
        .equal(student.department_organization)
        .select('id', 'pic')
        .getItem();

    if (academicCardConfiguration == null) {
        throw new DataNotFoundError(
            'The institute\'s academic student card configuration cannot be found or is inaccesible'
        );
    }
    
    // Improve
    // Get server from configuration file
    const server = context.getApplication().getConfiguration().getSourceAt('settings/server') ? context.getApplication().getConfiguration().getSourceAt('settings/server') : 0;

    return EscnFactory.getEscn(server, academicCardConfiguration.pic); 
}


export async function getEuropeanStudentIdentifier(context, student) {
    // Get student data
    student = await context.model('Student')
        .where('id')
        .equal(student)
        .expand('department')
        .select('id', 'department/organization', 'uniqueIdentifier')
        .getItem();

    if (student == null) {
        throw new DataNotFoundError(
            'The specified student cannot be found or is inaccesible'
        );
    }

    // Get the academic card configuration of the student's institute
    const academicCardConfiguration = await context.model('AcademicCardConfiguration')
        .where('institute')
        .equal(student.department_organization)
        .select('id', 'esiPrefix', 'sHO', 'countryCode')
        .getItem();

    if (academicCardConfiguration == null) {
        throw new DataNotFoundError(
            'The institute\'s academic student card configuration cannot be found or is inaccesible'
        );
    }
    
    // Construct esi
    const esi = `${academicCardConfiguration.esiPrefix}:${academicCardConfiguration.countryCode.toLowerCase()}:${academicCardConfiguration.sHO.toLowerCase()}:${student.uniqueIdentifier}`;

    return esi;
}

