import { EdmMapping } from '@themost/data/odata';
import { DataObject } from '@themost/data/data-object';

/**
 * @class
 
 * @property {number} id
 * @property {number} identifier
 * @property {string} additionalType
 * @property {string} alternateName
 * @property {string} description
 * @property {string} image
 * @property {string} name
 * @property {string} url
 * @property {Date} dateCreated
 * @property {Date} dateModified
 * @property {User|any} createdBy
 * @property {User|any} modifiedBy
 * @augments {DataObject}
 */
@EdmMapping.entityType('AcademicCardStatus')
class AcademicCardStatus extends DataObject {
    /**
     * @constructor
     */
    constructor() {
        super();
    }
}
module.exports = AcademicCardStatus;
