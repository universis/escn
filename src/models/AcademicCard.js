import { DataObject, EdmMapping } from '@themost/data';

/**
 * @class
 * 
 * @property {number} id
 * @property {number} escn
 * @property {Student|any} student
 * @property {string} esi
 * @property {string} cardUid
 * @property {string} isActive
 * @property {string} cardType
 * @property {Date} expiryDate
 * @property {string} nacn
 * @property {string} statusMessage
 */
@EdmMapping.entityType('AcademicCard')
class AcademicCard extends DataObject {
    constructor() {
        super();
    }
}

export {
    AcademicCard
}
