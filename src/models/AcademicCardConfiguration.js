import { DataObject, EdmMapping } from '@themost/data';

/**
 * @class 
 *
 * @property {number} id 
 * @property {Institute|any} Institute
 * @property {number} pic
 * @property {string} esiPrefix
 * @property {string} countryCode
 * @property {string} sHO
 */
@EdmMapping.entityType('AcademicCardConfiguration')
class AcademicCardConfiguration extends DataObject {
    constructor() {
        super();
    }
} 

export {
    AcademicCardConfiguration
}
