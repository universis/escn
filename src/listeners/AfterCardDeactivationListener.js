import { DataNotFoundError, DataError } from '@themost/common';

const superagent = require('superagent');

async function afterSaveAsync(event) {
    if (event.state !== 2) {
        return;
    }

    if (!event.target.hasOwnProperty.call(event.target, 'isActive') || event.target.isActive === null) {
        return;
    }

    const context = event.model.context;
    const acId = event.target.id;

    // Get academic card
    const academicCard = await context.model('AcademicCard')
        .where('id')
        .equal(acId)
        .select('escn', 'esi')
        .getItem();
    if (academicCard == null) {
        throw new DataNotFoundError('The specified academic card could not be found');
    }

    // Check card's previous status
    const previousCardIsActive = event.previous && event.previous.isActive;
    if (previousCardIsActive == null) {
        throw new DataError('E_PREVIOUS', 'The previous card status could not be determined');
    }
    
    // If card is getting deactivated 
    // or if card status was failed (5) and now is pending (1)
    // remove it from the ESC router
    if (event.target.isActive === false && (previousCardIsActive === true || (event.target.status === 1 && event.previous.status === 5))) {
        let cardStatus, statusMessage;
        await superagent
            .delete(`${context.getApplication().getConfiguration().getSourceAt('settings/auth/escRouter/api')}/students/${academicCard.esi}/cards/${academicCard.escn}`)
            .set('key', context.getApplication().getConfiguration().getSourceAt('settings/auth/escRouter/key'))
            .set('Content-Type', 'application/json')
            .then(cardStatus = 4)
            .catch((error) => {
                cardStatus = 5;
                statusMessage = error.response?._body?.error_description;
            });

        // Update card status to 4 (removed) or 5 (failed) depending on the result of the card post to the ESC router
        if (cardStatus === 4 || cardStatus === 5) {
            const cardUpdate = {
                id: acId,
                status: cardStatus,
                statusMessage: statusMessage
            }

            await context.model('AcademicCard').save(cardUpdate);
        }
    }
     
}

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function afterSave(event, callback) {
    return afterSaveAsync(event).then(() => {
        return callback();
    }).catch((err) => {
        return callback(err);
    });
}
