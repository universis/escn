async function afterSaveAsync(event) {
    if (event.state === 1) {
        const context = event.model.context;
        const studentId = event.target.id;
        const student = await context.model('Student')
            .where('id')
            .equal(studentId)
            .select('id', 'department', 'uniqueIdentifier')
            .getItem();
            
        if (student != null) {
            const newAcademicCard = {
                student: student,
                isActive: true
            }

            await context.model('AcademicCard').save(newAcademicCard);
        }
    }
}

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function afterSave(event, callback) {
    return afterSaveAsync(event).then(() => {
        return callback();
    }).catch((err) => {
        return callback(err);
    });
}
