import {DataNotFoundError, HttpError} from '@themost/common';
const superagent = require('superagent');

async function afterSaveAsync(event) {
    if (event.state !== 2) {
        return;
    }

    const context = event.model.context;

    if (!event.target.hasOwnProperty.call(event.target, 'email') || event.target.email === null) {
        return;
    }

    // If person field name is updated and previously was empty change active student academic card's status to 1 (pending) if there one
    if (event.previous && event.previous.email == null && event.target && event.target.email != null) {
        // Get active academic card (if one exists)
        const academicCard = await context.model('AcademicCard')
            .where('student/person')
            .equal(event.target.id)
            .and('isActive')
            .equal(true)
            .select('id', 'status')
            .expand('Student')
            .getItem();

        // If card is in draft state change it to pending
        if (academicCard && academicCard.status === 2) {
            const cardUpdate = {
                id: academicCard.id,
                status: 1
            }

            await context.model('AcademicCard').save(cardUpdate);
        }
    }

    // If person had an email that got updated and has an active student card, udpate student's email in ESC router 
    if (event.previous && event.previous.email != null && event.target && event.target.email != null && event.previous.email !== event.target.email) {
        // Retrieve person's academic students cards
        const academicCards = await context.model('AcademicCard')
            .where('student/person')
            .equal(event.target.id)
            .expand('student')
            .select('id', 'esi', 'isActive', 'expiryDate', 'student/person/email', 'student/department/organization')
            .getItems();

        if (!academicCards) {
            return;
        }

        // Find the active card if one exists
        let card = academicCards.find(card => card.isActive === true);
        let pic;
        
        // If there is an active card get institute PIC through academic card configurations model
        // else check if student is posted in router and get the PIC from the router's response
        if (card) {
            const academicCardConfiguration = await context.model('AcademicCardConfiguration')
                .where('institute')
                .equal(card.student_department_organization)
                .select('id', 'pic')
                .getItem()

            if (!academicCardConfiguration || !academicCardConfiguration.pic) {
                throw new DataNotFoundError('The academic card configuration for the specified institute was not found');
            }

            pic = academicCardConfiguration.pic;
        } else {
            card = academicCards[0];
            await superagent
                .get(`${context.getApplication().getConfiguration().getSourceAt('settings/auth/escRouter/api')}/students/${card.esi}`)
                .set('key', context.getApplication().getConfiguration().getSourceAt('settings/auth/escRouter/key'))
                .set('Content-Type', 'application/json')
                .then((res) => {
                    pic = res._body?.picInstitutionCode;
                });
        }

        if (!pic) {
            throw new DataNotFoundError('Person\'s institute PIC could not be retrieved');
        }
       
        // Update student in the router
        await superagent
            .put(`${context.getApplication().getConfiguration().getSourceAt('settings/auth/escRouter/api')}/students/${card.esi}`)
            .send({ 
                europeanStudentIdentifier: card.esi,
                emailAddress: card.student_person_email,
                picInstitutionCode: pic,
                expiryDate: card.expiryDate
            })
            .set('key', context.getApplication().getConfiguration().getSourceAt('settings/auth/escRouter/key'))
            .set('Content-Type', 'application/json')
            .catch((error) => {
                throw new HttpError(
                    error.status,
                    error.response?._body?.error || error.statusMessage,
                    error.response?._body?.error_description
                )
            });
    }
}


/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function afterSave(event, callback) {
    return afterSaveAsync(event).then(() => {
        return callback();
    }).catch((err) => {
        return callback(err);
    });
}
