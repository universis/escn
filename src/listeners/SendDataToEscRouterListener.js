import { DataNotFoundError, HttpError } from '@themost/common';
const superagent = require('superagent');

async function afterSaveAsync(event) {
    if (event.state === 1 || event.state === 2) {
        // Don't do anything if card status is not pending 
        if (event.target.status !== 1) {
            return;
        }

        const context = event.model.context;
        const acId = event.target.id;

        // Get academic student card
        const academicCard = await context.model('AcademicCard')
            .where('id')
            .equal(acId)
            .and('isActive')
            .equal(true)
            .select('escn', 'esi', 'student', 'expiryDate')
            .getItem();

        if (academicCard != null) {
            // Check if user is added in the esc router
            await superagent
                .get(`${context.getApplication().getConfiguration().getSourceAt('settings/auth/escRouter/api')}/students/${academicCard.esi}`)
                .set('key', context.getApplication().getConfiguration().getSourceAt('settings/auth/escRouter/key'))
                .set('Content-Type', 'application/json')
                .catch(async (error) => {
                    if (error.status === 404) {
                        // If student doesn't exist in the esc router add them 
                        const student = await context.model('Student')
                            .where('id').equal(academicCard.student)
                            .expand('department', 'person')
                            .select('department/organization', 'person/email')
                            .getItem();
                        
                        if (student != null) {
                            const academicCardConfiguration = await context.model('AcademicCardConfiguration')
                                .where('institute')
                                .equal(student.department_organization)
                                .select('pic')
                                .getItem();
                            
                            // Add the new student to the esc router
                            await superagent
                                .post(`${context.getApplication().getConfiguration().getSourceAt('settings/auth/escRouter/api')}/students`)
                                .send({ 
                                    europeanStudentIdentifier: academicCard.esi,
                                    picInstitutionCode: academicCardConfiguration.pic,
                                    emailAddress: student.person_email,
                                    expiryDate: academicCard.expiryDate
                                })
                                .set('key', context.getApplication().getConfiguration().getSourceAt('settings/auth/escRouter/key'))
                                .set('Content-Type', 'application/json')
                                .catch((error) => {
                                    throw new HttpError(
                                        error.status,
                                        error.response?._body?.error || error.statusMessage,
                                        error.response?._body?.error_description
                                    )
                                });                
                        }
                    } else {
                        throw new DataNotFoundError(
                            error.response?._body?.error || error.statusMessage,
                            error.response?._body?.error_description
                        );
                    }
                });

            let cardStatus, statusMessage;
            // Add the new card to the esc router
            await superagent
                .post(`${context.getApplication().getConfiguration().getSourceAt('settings/auth/escRouter/api')}/students/${academicCard.esi}/cards`)
                .send({ 
                    europeanStudentCardNumber: academicCard.escn
                })
                .set('key', context.getApplication().getConfiguration().getSourceAt('settings/auth/escRouter/key'))
                .set('Content-Type', 'application/json')
                .then(cardStatus = 3)
                .catch((error) => {
                    cardStatus = 5;
                    statusMessage = error.response?._body?.error_description;
                });

            // Update card status to 3 (completed) or 5 (failed) depending on the result of the card post to the ESC router
            if (cardStatus === 3 || cardStatus === 5) {
                const cardUpdate = {
                    id: acId,
                    status: cardStatus,
                    statusMessage: statusMessage
                }

                await context.model('AcademicCard').save(cardUpdate);
            }
        }
    }
}
/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function afterSave(event, callback) {
    return afterSaveAsync(event).then(() => {
        return callback();
    }).catch((err) => {
        return callback(err);
    });
}
