import { DataError, HttpError } from '@themost/common';
const superagent = require('superagent');

async function afterSaveAsync(event) {
    if (event.state !== 2) {
        return;
    }

    if (!event.target.hasOwnProperty.call(event.target, 'studentStatus') || event.target.studentStatus === null) {
        return;
    }

    const context = event.model.context;
    const studentId = event.target.id;

    // Get student's data
    const student = await context.model('Student')
        .where('id')
        .equal(studentId)
        .select('id', 'department', 'uniqueIdentifier')
        .getItem();
    if (student === null) {
        return;
    }

    // Get student's current status
    const studentStatus = await context.model('StudentStatus').find(event.target.studentStatus).getItem();
    if (studentStatus === null) {
        return;
    }

    // Get student's previous status
    const previousStudentStatus = event.previous && event.previous.studentStatus;
    if (previousStudentStatus === null) {
        throw new DataError('E_PREVIOUS', 'The previous student status can not be determined');
    }

    // If student was declared and now is gratuated or
    // if student was active and now is erased,
    // deactivate the active card if there is one 
    if ((studentStatus.alternateName === 'gratuated' && previousStudentStatus.alternateName === 'declared') ||
        (previousStudentStatus.alternateName === 'active' && studentStatus.alternateName === 'erased')) {
        const esc = await context.model('AcademicCard')
            .where('student')
            .equal(studentId)
            .and('isActive')
            .equal(true)
            .select('id', 'esi')
            .getItem()

        if (esc) {
            const acUpdate = {
                id: esc.id,
                student: student,
                isActive: false
            }

            await context.model('AcademicCard').save(acUpdate);

            // Delete student from router when student status becomes graduated or erased
            superagent
                .delete(`${context.getApplication().getConfiguration().getSourceAt('settings/auth/escRouter/api')}/students/${esc.esi}`)
                .set('key', context.getApplication().getConfiguration().getSourceAt('settings/auth/escRouter/key'))
                .set('Content-Type', 'application/json')
                .catch((error) => {
                    throw new HttpError(
                        error.status,
                        error.response?._body?.error || error.statusMessage,
                        error.response?._body?.error_description
                    )
                });
        }
    }
}


/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function afterSave(event, callback) {
    return afterSaveAsync(event).then(() => {
        return callback();
    }).catch((err) => {
        return callback(err);
    });
}
