/**
 * @param {DataEventArgs} event
 */
async function beforeSaveAsync(event) {
    if (event.state === 1 || event.state === 2) {
        const context = event.model.context;
        // Don't do anything if card doesn't have a student attached to it
        if (!event.target.hasOwnProperty.call(event.target, 'student')) {
            return;
        }
        
        // Don't do anything if card is deactivated
        if (event.target.hasOwnProperty.call(event.target, 'isActive') && event.target.isActive === false) {
            return;
        }

        if (!event.target.hasOwnProperty.call(event.target, 'status')) {
            // Get student's email
            const student = await context.model('Student')
                .where('id')
                .equal(event.target.student)
                .expand('person')
                .select('person/email')
                .getItem();

            // If student has a registered email then make status 1 (pending)
            // else 2 (draft)
            let cardStatus = 1;
            if (student && student.person_email == null) {
                cardStatus = 2;
            }

            // Insert card status to the event
            event.target.status = cardStatus;
        }

        if (!event.target.hasOwnProperty.call(event.target, 'expiryDate')) {
            // Get student data
            const studentData = await context.model('Student')
                .where('id')
                .equal(event.target.student)
                .expand('department')
                .select('department/totalSemesters', 'inscriptionDate')
                .getItem();

            // Construct expiry date as student's iscriptionDate + anticipated years of study + 2 years
            if (studentData) {
                let expiryDate = new Date(studentData.inscriptionDate);
                expiryDate.setFullYear(expiryDate.getFullYear() + Math.ceil(studentData.department_totalSemesters/2) + 2);

                event.target.expiryDate = expiryDate;
            }
        }
    }
}

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function beforeSave(event, callback) {
    return beforeSaveAsync(event).then(() => {
        return callback();
    }).catch(err => {
        return callback(err);
    });
}
