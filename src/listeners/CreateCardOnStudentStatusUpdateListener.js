import { DataError } from '@themost/common';

async function afterSaveAsync(event) {
    if (event.state !== 2) {
        return;
    }

    if (!event.target.hasOwnProperty.call(event.target, 'studentStatus') || event.target.studentStatus === null) {
        return;
    }

    const context = event.model.context;
    const studentId = event.target.id;

    // Get student
    const student = await context.model('Student')
        .where('id')
        .equal(studentId)
        .select('id', 'department', 'uniqueIdentifier', 'person')
        .expand('person')
        .getItem();
    if (student === null) {
        return;
    }

    // Get student current status
    const studentStatus = await context.model('StudentStatus')
        .find(event.target.studentStatus)
        .getItem();
    if (studentStatus === null) {
        return;
    }

    // Get student previous status
    const previousStudentStatus = event.previous && event.previous.studentStatus;
    if (previousStudentStatus === null) {
        throw new DataError('E_PREVIOUS', 'The previous student status can not be determined');
    }

    // If student was graduated and now is declared or
    // if student was erased and now is active,
    // create a new active academic card 
    if ((studentStatus.alternateName === 'declared' && previousStudentStatus.alternateName === 'graduated') ||
        (studentStatus.alternateName === 'active' && previousStudentStatus.alternateName === 'erased')) {

        const newAcademicCard = {
            student: student,
            isActive: true
        };

        await context.model('AcademicCard').save(newAcademicCard);
    }
}


/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function afterSave(event, callback) {
    return afterSaveAsync(event).then(() => {
        return callback();
    }).catch((err) => {
        return callback(err);
    });
}

