import { ApplicationService } from '@themost/common';
import { SchemaLoaderStrategy } from '@themost/data';
import path from 'path';

export class PersonReplacer extends ApplicationService {
    constructor(app) {
        super(app);
    }

    apply() {
        // get schema loader
        const schemaLoader = this.getApplication().getConfiguration().getStrategy(SchemaLoaderStrategy);
        // get model definition
        const model = schemaLoader.getModelDefinition('Person');

        model.eventListeners = model.eventListeners || [];
        
        // add the listeners
        model.eventListeners.push(
            {type: path.resolve(__dirname, 'listeners/AfterSaveUpdatePersonListener')}
        );
        
        schemaLoader.setModelDefinition(model);
    }
}

