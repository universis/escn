# @universis/escn

Universis API server plugin for managing the European student cards

## Usage

Install plugin

    npm i @universis/escn

## Configure

Register `@universis/escn#EscnService` in application services:

    # app.production.json

    "services": [
        ...,
        {
            "serviceType": "@universis/escn#EscnService"
        }
    ]

Add `@universis/escn#EscnSchemaLoader` to schema loaders:

    # app.production.json
    
    {
        "settings": {
            "schema": {
                "loaders": [
                    ...,
                    {
                        "loaderType": "@universis/escn#EscnSchemaLoader"
                    }
                ]
            }
        }
    }

Add `escRouter` in auth section with the ESC Router API URI (without trailing slash) and the API key:

    # app.production.json
    
    {
        "settings": {
            "auth": {
                ...,
                "escRouter": {
                    "api": ""
                    "key": ""
                }
            }
        }
    }
